cmake_minimum_required ( VERSION 3.10 )

project ( rhyme_samr_boundary_condition )
enable_language ( Fortran )
add_compile_options ( -Wall -Wextra -std=f2008 )


set ( INSTALL_LIB_DIR ${CMAKE_BINARY_DIR}/lib CACHE PATH "Lib directory" )
set ( INSTALL_INCLUDE_DIR ${CMAKE_BINARY_DIR}/include CACHE PATH "Include directory" )
set ( CMAKE_Fortran_MODULE_DIRECTORY ${INSTALL_INCLUDE_DIR} )


set ( deps )

set ( rhyme_deps
  rhyme_hydro_base@master
  rhyme_samr@master
)

foreach ( rhyme_dep ${rhyme_deps} )

  string ( REGEX REPLACE "@(.*)" "" package ${rhyme_dep})
  string ( REGEX REPLACE "(.*)@" "" commit ${rhyme_dep})

    include ( ExternalProject )

    set ( lib_build_dir ${PROJECT_BINARY_DIR}/${package}/build )
    set ( lib_src_dir ${PROJECT_BINARY_DIR}/${package}/pkg )

    ExternalProject_Add ( ${package}
      GIT_REPOSITORY https://gitlab.com/rhyme-org/${package}.git
      GIT_TAG ${commit}
      SOURCE_DIR ${lib_src_dir}
      BINARY_DIR ${lib_build_dir}
      UPDATE_DISCONNECTED ON
      INSTALL_COMMAND ""
    )

    include_directories ( ${lib_build_dir}/include )

    set ( lib_name lib${package}.a )
    list ( APPEND deps ${lib_build_dir}/${lib_name} )
endforeach ( rhyme_dep )


add_library ( ${PROJECT_NAME} STATIC src/rhyme_samr_boundary_condition.f90 )
target_link_libraries ( ${PROJECT_NAME} PRIVATE ${deps} )


add_subdirectory ( tests )
enable_testing ()
